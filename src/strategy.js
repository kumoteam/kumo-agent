const ActionStrategy = {
  ID: 'ID',
  NAME: 'NAME'
};

module.exports = ActionStrategy;

class Agent {
  constructor(url, uuid);
  connect();
  actionStrategy(val);
  defaultObserver(obs);
  on(key, callback);
  emit(event);
  message(message);
  disconnect();
}