class AlreadyConnectedError extends Error {
  constructor() {
    super('Agent is already connected');
    this.name = 'AlreadyConnectedError';
  }
}

class NotConnectedError extends Error {
  constructor() {
    super('Agent is not connected');
    this.name = 'NotConnectedError';
  }
}

class ObserverTypeMismatchError extends Error {
  constructor() {
    super('Observer is not a function');
    this.name = 'ObserverTypeMismatchError';
  }
}

module.exports.AlreadyConnectedError = AlreadyConnectedError;
module.exports.NotConnectedError = NotConnectedError;
module.exports.ObserverTypeMismatchError = ObserverTypeMismatchError;

module.exports.Error = {
  AlreadyConnectedError,
  NotConnectedError,
  ObserverTypeMismatchError
};