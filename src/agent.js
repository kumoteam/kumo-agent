const io = require('socket.io-client');
const ActionStrategy = require('./strategy');
const AlreadyConnectedError = require('./error').AlreadyConnectedError;
const NotConnectedError = require('./error').NotConnectedError;
const ObserverTypeMismatchError = require('./error').ObserverTypeMismatchError;

module.exports = function (uri, uuid) {
  this.uri = uri;
  this.uuid = uuid;

  let strategy = 'ID';
  let isConnected = false;
  let observer = {};

  let defaultObserver = (action) => {
    console.log(`[${this.uuid}][action]: ${JSON.stringify(action)}`);
  };

  this.connect = () => {
    if (!isConnected) {
      isConnected = true;

      this.socket = io.connect(this.uri);

      this.socket.on('handshake', () => {
        this.socket.emit('handshake', {uuid: this.uuid});
      });

      this.socket.on('action', (action) => {
        let currentObserver;

        if (strategy === ActionStrategy.NAME) {
          currentObserver = observer[`${action.name}`];
        } else {
          currentObserver = observer[`${action.id}`];
        }

        if (typeof currentObserver === 'function') {
          currentObserver(action);
        } else {
          defaultObserver(action);
        }
      });

    } else {
      throw new AlreadyConnectedError();
    }
  };

  this.actionStrategy = (val) => {
    switch (val) {
      case ActionStrategy.NAME:
        strategy = ActionStrategy.NAME;
        break;
      case ActionStrategy.ID:
      default:
        strategy = ActionStrategy.ID;
        break;
    }
  };

  this.defaultObserver = (obs) => {
    if (typeof obs === 'function') {
      defaultObserver = obs;
    } else {
      throw new ObserverTypeMismatchError();
    }
  };

  this.on = (key, callback) => {
    if (typeof callback === 'function') {
      observer[key] = callback;
    } else {
      throw new ObserverTypeMismatchError();
    }
  };

  this.emit = (event) => {
    if (isConnected) {
      this.socket.emit('event', event);
    } else {
      throw new NotConnectedError();
    }
  };

  this.message = (message) => {
    if (isConnected) {
      this.socket.emit('message', message);
    } else {
      throw new NotConnectedError();
    }

  };

  this.disconnect = () => {
    if (isConnected) {
      this.socket.disconnect();
      isConnected = false;
    } else {
      throw new NotConnectedError();
    }
  };
};
